import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import Footer from './static/Footer';

// Components
import Navigation from './static/Navigation';
const Home = () => {
    return (
        <>
            <Navigation />
            <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-2" data-slide-to="1"></li>
                    <li data-target="#carousel-example-2" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                    <div class="view">
                        <img style={{ height: '72vh' }} class="d-block w-100" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1612247939/cannabis/img_t2jkr0.webp"
                        alt="First slide" />
                        <div class="mask rgba-black-light"></div>
                    </div>
                    <div class="carousel-caption mb-4">
                        <h2 class="h3-responsive font-weight-bold">Out of the Ordinary</h2>
                        <h4>Break free from the everyday with these specialty carts</h4>
                        <a href="/products" className="btn btn-lg btn-outline-light">Shop Now</a>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="view">
                        <img style={{ height: '72vh' }} class="d-block w-100" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1612248754/cannabis/photo-1564171149171-1b0e8c0e0bef_cnjhgj.jpg"
                        alt="Second slide" />
                        <div class="mask rgba-black-strong"></div>
                    </div>
                    <div class="carousel-caption mb-4">
                        <h2 class="h3-responsive font-weight-bold">Out of the Ordinary</h2>
                        <h4>Break free from the everyday with these specialty carts</h4>
                        <a href="/products" className="btn btn-lg btn-outline-light">Shop Now</a>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="view">
                        <img style={{ height: '72vh' }} class="d-block w-100" src="https://i.cbc.ca/1.4882503.1569515303!/cpImage/httpImage/cannabis-border-20181024.jpg"
                        alt="Third slide" />
                        <div class="mask rgba-black-slight"></div>
                    </div>
                    <div class="carousel-caption mb-4">
                        <h2 class="h3-responsive font-weight-bold">Out of the Ordinary</h2>
                        <h4>Break free from the everyday with these specialty carts</h4>
                        <a href="/products" className="btn btn-lg btn-outline-light">Shop Now</a>
                    </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <section className="container mt-4 mb-4">
                <div class="row mt-4 pt-1">
                    <div class="col-lg-12 col-md-12 mb-3 mb-md-12 pb-lg-2">
                        <div class="view zoom z-depth-1">
                            <img style={{ width: '100%' }} src="https://images.wallpaperscraft.com/image/leaves_bushes_green_174515_1920x1080.jpg" class="img-fluid" alt="sample image" />
                            <div class="mask">
                                <div class="dark-grey-text d-flex align-items-center pt-4 ml-lg-3 pl-lg-3 pl-md-5">
                                    <div>
                                        <a><span class="badge badge-danger text-light">BEST DELIVERY</span></a>
                                        <h1 class="card-title font-weight-bold pt-2"><strong className="text-light">Delivery under 24 Hours</strong></h1>
                                        <p class="hidden show-ud-up text-light">We make sure that you will get the right product on right time</p>
                                        <a href="/products?q=indica" class="btn btn-outline-light btn-lg btn-rounded clearfix d-none d-md-inline-block waves-effect waves-light">Shop Now</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>


            <section className="container mt-4 w-75 mb-4">
                <div className="row">
                    <div className="col">
                        <div class="card card-image" style={{ backgroundImage: ' url(https://leafly-cms-production.imgix.net/wp-content/uploads/2020/07/22141445/purple-strains.jpg)' }}>
                        <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                            <div>
                            <h3 class="pt-2"><strong style={{ color: '#fff' }}>How Are Topicals Made?</strong></h3>
                            <p>Aphria and 48North gave us the inside scoop on how innovative new cannabis-infused creams, lotions and oils are whipped up.</p>
                            <button style={{ backgroundColor: '#fff', color: '#000' }} type="button" class="btn btn-block font-weight-bold">Learn More</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col">
                        <div class="card card-image" style={{ backgroundImage: ' url(https://c4.wallpaperflare.com/wallpaper/684/115/821/420-cannabis-marijuana-weed-wallpaper-preview.jpg)' }}>
                        <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                            <div>
                            <h3 class="pt-2"><strong style={{ color: '#fff' }}>Featured Flower: Tangerine Dream</strong></h3>
                            <p>Like its namesake, this customer favourite boasts an orange hue and a strong citrusy aroma.</p>
                            <button style={{ backgroundColor: '#fff', color: '#000' }} type="button" class="btn btn-block font-weight-bold">Learn More</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>


            <section className="container" style={{ marginTop: '10vh' }}>
                <h2 className="text-center">
                    <center>
                        <img className="w-25 mt-4 mb-4" src="//cdn.shopify.com/s/files/1/2636/1928/files/OCS_EN_LOGO_BLK_SM_nav_600x.png?v=1560138074" alt="Work" />
                    </center>
                    <br />
                    <b className="mb-2 mt-4">
                    Marking One Year of Cannabis 2.0
                    </b>
                    <h6>
                    Last January, the first vapes, chocolates and soft chews arrived on OCS.ca. Now we have over 350 extracts, edibles and topicals to choose from
                    </h6>
                </h2>

                <br />
                <Grid container spacing={6}>
                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00824164000436_00_compressed_300206_300x300_crop_center.jpg?v=1610642626" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>1 G CARTRIDGES</b>
                                </a>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00846067000153_00_compressed_310020_300x300_crop_center.jpg?v=1608684061" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>SOLVENTLESS CONCENTRATES</b>
                                </a>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00628582001187_00_compressed_310021_300x300_crop_center.jpg?v=1608831585" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>SOLVENT-EXTRACTED CONCENTRATES</b>
                                </a>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Grid container spacing={6}>
                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00628639000538_00_compressed_310036_300x300_crop_center.jpg?v=1608740854" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>ISOLATES AND DISTILLATES</b>
                                </a>
                            </div>
                        </div>
                    </Grid>

                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00842865000531_A1C1_compress_340001_300x300_crop_center.jpg?v=1609270608" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>TOPICALS</b>
                                </a>
                            </div>
                        </div>
                    </Grid>

                    <Grid item xs={4}>
                        <div className="card">
                            <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00688083002748_a1c1_compress_320001_c809e894-4b4b-43a9-bfb1-9959ef931875_300x300_crop_center.jpg?v=1612215434" />
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-outline-success">
                                    <b>SPARKLING WATER</b>
                                </a>
                            </div>
                        </div>
                    </Grid>

                </Grid>
            </section>

            <section className="container" style={{ marginTop: '10vh' }}>
                <div class="row">
                <div class="col-12">
                    <div class="view  z-depth-1">
                        <img style={{ width: '80vh' }} src="https://www.pngitem.com/pimgs/m/112-1120209_pot-leaf-transparent-png-hemp-leaf-transparent-background.png" class="img-fluid" alt="sample image" />
                        <div class="mask rgba-stylish-slight">
                            <div class="dark-grey-text text-right pt-lg-5 pt-md-1 mr-5 pr-md-4 pr-0">
                                <div>
                                    <h2 class="mt-4 card-title font-weight-bold pt-md-3 pt-1">
                                        <strong>Kick Off the Big Game
                                        </strong>
                                    </h2>
                                    <p class="pb-lg-3 pb-md-1 clearfix d-none d-md-block">Put these high-THC edibles and beverages in your starting lineup</p>
                                    <a class="btn btn-outline-success btn-lg">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>

            <section className="container" style={{ marginTop: '10vh' }}>
                <h2 className="text-center">
                    <b className="mb-2">
                    Bestsellers of 2020
                    </b>
                    <h6>
                    It was a year without many bests, but plenty of bestsellers
                    </h6>
                </h2>

                <center>
                    <Splide
                        options={{
                            type      : 'loop',
                            width     : 1100,
                            perPage   : 4,
                            perMove   : 1,
                            gap       : '4rem',
                            pagination: false,
                        }}
                    
                    >
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                        <SplideSlide>
                            <img src="https://www.pngitem.com/pimgs/m/50-507689_pot-leaf-no-background-transparent-background-cannabis-leaf.png" alt="Image 1"/>
                            <h5 className="mt-2">
                                <b>REDCEN</b>
                            </h5>
                            <h4>
                                <b>
                                    Cold Creek Kush
                                </b>
                            </h4>
                            <h5>
                                from $5.99/g <br /> Taxes Included
                            </h5>
                            <h5>
                                Sativa Dominant
                            </h5>
                            <h5>
                                <b>THC </b> 17.0 - 23.0%
                            </h5>
                            <h5>
                                <b>CDB </b> 0.0 - 1.5%
                            </h5>
                        </SplideSlide>
                    </Splide>
                    <button className="btn btn-outline-success">
                        Shop All
                    </button>
                </center>
            </section>


            <section className="container" style={{ marginTop: '10vh' }}>
                <div class="view zoom  z-depth-1">
                    <img style={{ width: '100%' }} src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1612255258/cannabis/816276_cph7vj.jpg" class="img-fluid" alt="sample image" /> 
                    <div class="mask">
                        <div class="d-flex align-items-center pt-3 pl-4">
                            <div style={{ marginTop: '10vh' }}>
                                <h1 class="card-title font-weight-bold pt-2 display-4"><strong style={{ color: '#fff' }}>Grab the best deals on Cannabist Today</strong></h1>
                                <p style={{ color: '#fff' }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                                <a class="btn btn-outline-light btn-lg">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <br />
            <Footer />
        </>
    )
}

export default Home
