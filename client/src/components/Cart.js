import React from 'react';
import Grid from '@material-ui/core/Grid';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
import { indica, sativa } from './items/indica';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ProductList = ({ deleteItem, p, settotalprice, product }) => {
    var sizes = product.product.size[0];
    settotalprice(p);
    return (
        <Grid item xs={3}>
            <div className="card">
                <div className="row p-2">
                    <div className="col h6 font-weight-bold text-center">
                        THC <br />
                        {`${product.product.thc} %`}
                    </div>
                    <div className="col h6 font-weight-bold text-center">
                        CBD <br />
                        {`${product.product.cbd} %`}
                    </div>
                </div>
                <center>
                    <img style={{ width: '75%' }} src={product.product.photo} />
                </center>
                <center className="text-dark">
                    <h6 className="font-weight-bold">
                        {product.product.type}
                    </h6>
                    <h5 className="font-weight-bold">
                        {product.product.name}
                    </h5>
                    <h6 className="font-weight-bold">
                        {product.product.category}
                    </h6>
                    <h6 className="font-weight-bold">
                        {sizes} /g
                    </h6>
                </center>
                <center>
                    <h5 className="font-weight-bold">
                        {`$ ${product.product.price}/g`}
                    </h5>
                </center>
                <div class="card-body">
                    <button onClick={() => deleteItem(product._id)} class="btn btn-block btn-outline-danger">
                        <b>Delete Item</b>
                    </button>
                </div>
            </div>
        </Grid>
    )
}


const Products = ({ location }) => {
    const [message, setmessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState('');
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const refreshList = () => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }

    const deleteItem = (documentId) => {
        setLoading(true);
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            var items = doc.data().items;
            axios.get(`${API_SERVICE}/api/v1/main/removeitemtocart/${documentId}`)
                .then((response) => {
                    if (response.status === 200) {
                        refreshList();
                        handleClick();
                        setmessage('Item Removed from Cart');
                        items = items - 1;
                        docRef.set({
                            items 
                        }, { merge: true });
                    } 
                }).catch(err => console.log(err));
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const showProductList = () => {
        var p = 0;
        return products.map(product => {
            p = product.product.price + p;
            return <ProductList deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }
    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    {message}
                </Alert>
            </Snackbar>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Cart</h2>
                    <p className="lead font-weight-bold text-success h5">
                        {
                            products && products.length ? (
                            <>    
                                {`Total Price $ ${totalprice} CAD`}
                                <br />
                                <a href="/checkout" className="btn btn-outline-success">
                                    Proceed to Checkout
                                </a>
                            </>
                            ) : (
                                `Total Price $ 0 CAD`
                            )
                        }
                    </p>
                </div>
            </div>
            <section className="m-4">
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <Grid container spacing={3}>
                                {showProductList()}
                            </Grid>
                            <br />
                            <center>
                                <a href="/checkout" className="btn w-50 btn-lg btn-success">
                                    Proceed to Checkout
                                </a>
                            </center>
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        Empty Cart
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
            </section>
            <Footer />
        </>
    )
}

export default Products
