import React from 'react';
import Grid from '@material-ui/core/Grid';
import queryString from 'query-string';

// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
import { indica, sativa } from './items/indica';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ProductList = ({ setsize, addItem, product }) => {
    var sizes = product.size;
    return (
        <Grid item xs={3}>
            <div className="card">
                <div className="row p-2">
                    <div className="col h6 font-weight-bold text-center">
                        THC <br />
                        {`${product.thc} %`}
                    </div>
                    <div className="col h6 font-weight-bold text-center">
                        CBD <br />
                        {`${product.cbd} %`}
                    </div>
                </div>
                <center>
                    <img style={{ width: '75%' }} src={product.photo} />
                </center>
                <center className="text-dark">
                    <h6 className="font-weight-bold">
                        {product.type}
                    </h6>
                    <h5 className="font-weight-bold">
                        {product.name}
                    </h5>
                    <h6 className="font-weight-bold">
                        {product.category}
                    </h6>
                </center>
                <span className="m-4">
                    <select onChange={(e) => setsize(e.target.value)} className="form-control" aria-label="Default select example">
                        <option selected>Select Size</option>
                        {sizes.map((size) => (
                            <option value={size}>{size} g</option>
                        ))}
                    </select>
                </span>
                <center>
                    <h5 className="font-weight-bold">
                        {`$ ${product.price}/g`}
                    </h5>
                </center>
                <div class="card-body">
                    <button onClick={() => addItem(product)} class="btn btn-block btn-outline-success">
                        <b>Add to cart</b>
                    </button>
                    <button class="btn btn-block btn-light mt-2">
                        <b>View Now</b>
                    </button>
                </div>
            </div>
        </Grid>
    )
}


const Products = ({ location }) => {
    const [query, setquery] = React.useState('');
    const [heading, setheading] = React.useState('');
    const [subheading, setsubheading] = React.useState('');

    const [expanded, setExpanded] = React.useState(false);
    const [value, setValue] = React.useState([0, 37]);
    const [message, setmessage] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [filter, setfilter] = React.useState('');
    const [size, setsize] = React.useState('');

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const primaryOptions = {
        type      : 'loop',
        height     : 350,
        perPage   : 1,
        perMove   : 1,
        gap       : '1rem',
        autoplay     : true,
        pagination: false,
    };


    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        setquery(q);
        if (q === 'indica') {
            setheading('Indica-Dominant');
            setsubheading('Shop our collection of indica-dominant, high-THC and high-CBD products.');
        } else if (q === 'sativa') {
            setheading('Sativa-Dominant');
            setsubheading('');
        }
    }, []);


    const addItem = (product) => {
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            if (doc.exists) {
                var items = doc.data().items;
                if (items === 10000) {
                    handleClick();
                    setmessage('Maximum Items in Cart Exceeded');
                } else {
                    // Send Item to the Card in Database
                    var uploadData = {
                        productId: product._id,
                        userId: uid,
                        size: size,
                        product
                    }
                    axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                        .then((response) => {
                            if (response.status === 200) {
                                handleClick();
                                setmessage('Item Added to Cart');
                                items = items + 1;
                                docRef.set({
                                    items 
                                }, { merge: true });
                            } else if (response.status === 201) {
                                handleClick();
                                setmessage('Item Already Added to Cart');
                            }
                        }).catch(err => console.log(err));
                }
            } else {
                docRef.set({
                    items: 1 
                }, { merge: true });
                var uploadData = {
                    productId: product._id,
                    userId: uid,
                    size: size,
                    product
                }
                axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                    .then((response) => {
                        if (response.status === 200) {
                            handleClick();
                            setmessage('Item Added to Cart');
                        } 
                    }).catch(err => console.log(err));
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const showProductList = (q) => {
        if (q === 'indica') {
            return indica.map(product => {
                return <ProductList setsize={setsize} addItem={addItem} product={product} />
            })
        } else if (q === 'sativa') {
            return sativa.map(product => {
                return <ProductList setsize={setsize} addItem={addItem} product={product} />
            })
        } else {
            return sativa.map(product => {
                return <ProductList setsize={setsize} addItem={addItem} product={product} />
            })
        }
    }

    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    {message}
                </Alert>
            </Snackbar>

            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">{heading}</h2>
                    <p className="lead font-weight-bold">
                        {subheading}
                    </p>
                </div>
            </div>
            <section className="m-4">
                <Grid container spacing={3}>
                    {showProductList(query)}
                </Grid>
            </section>
            <Footer />
        </>
    )
}

export default Products
