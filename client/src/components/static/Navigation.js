import React from 'react';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import clsx from 'clsx';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { firestore } from '../../Firebase/index';
import { googleProvider, facebookProvider, auth } from "../../Firebase/index";
import Avatar from '@material-ui/core/Avatar';
const useStyles = makeStyles({
    list: {
      width: 400,
    },
    fullList: {
      width: 'auto',
    },
});

const Navigation = () => {
    const classes = useStyles();
    const [state, setState] = React.useState({
        right: false
    });

    const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
        return;
    }

    setState({ ...state, [anchor]: open });
    };


    const [open, setOpen] = React.useState(false);
    const [totalItems, setTotalItems] = React.useState(0);
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [user, setUser] = React.useState({});
    const [userlogin, setUserLogin] = React.useState();

    

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const [openSignUp, setOpenSignUp] = React.useState(false);
    const handleClickOpenSignUp = () => {
        setOpenSignUp(true);
    };
    const handleCloseSignUp = () => {
        setOpenSignUp(false);
    };

    const registerOpen = option => {
        if ( option === "Register" ) {
        handleClose();
        handleClickOpenSignUp();
        } else {
        handleCloseSignUp();
        handleClickOpen();
        }
    }

    const signIn = () => {
        auth.signInWithPopup(googleProvider).then((user) => {
        sessionStorage.setItem("userId", user.uid);
        sessionStorage.setItem("userEmail", user.email);
        sessionStorage.setItem("login", true);
        setUserLogin(true);
        handleClose();
        var user2 = auth.currentUser;
        if (user2) {
            cartItemFetch(user2.uid);
        } 
        }).catch(err => console.log(err));
    }

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
        if (user) {
            setUser(user);
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            cartItemFetch(user.uid);
        } else {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setUserLogin(false);
        }
        });
    }, []);

    React.useEffect(() => {
        if (sessionStorage.getItem("login")) {
        var uid = sessionStorage.getItem("userId");
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
        }
    }, []);

    const cartItemFetch = (uid) => {
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
    }

    const logOut = () => {
        auth.signOut().then(function() {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setTotalItems(0);
            setUserLogin(false);
        }).catch(function(error) {
            console.log(error);
        });
    }


    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
          <List>
            <ListItem button>
                <h1>
                    <center className="font-weight-bold text-center">
                        Checkout
                    </center>
                </h1>
            </ListItem>
            <ListItem button>
                <div className="card card-body">
                    <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00629108141141_00_compress_101306_6be9f883-3f68-4732-97c9-d310ee5227bb_480x.jpg" style={{ width: '10%', float: 'left' }} />
                    <h4 style={{ color: '#000' }}>Daily Special Indica</h4>
                    <h5>$ 4.46</h5>
                </div>
            </ListItem>
            <ListItem button>
                <div className="card card-body">
                    <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00827885003618_00_compress_101269_e67eea82-62ad-40fe-851c-f50185beee0d_480x.jpg" style={{ width: '10%', float: 'left' }} />
                    <h4 style={{ color: '#000' }}>Hybrid</h4>
                    <h5>$ 2.00</h5>
                </div>
            </ListItem>
            <ListItem button>
                <div className="card card-body">
                    <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00697238113185_00_compress_101789_480x.jpg" style={{ width: '10%', float: 'left' }} />
                    <h4 style={{ color: '#000' }}>OS.INDICA</h4>
                    <h5>$ 4.29</h5>
                </div>
            </ListItem>
            <ListItem button>
                <div className="card card-body">
                    <img src="//cdn.shopify.com/s/files/1/2636/1928/products/00629108141141_00_compress_101306_6be9f883-3f68-4732-97c9-d310ee5227bb_480x.jpg" style={{ width: '10%', float: 'left' }} />
                    <h4 style={{ color: '#000' }}>Daily Special Indica</h4>
                    <h5>$ 4.46</h5>
                </div>
            </ListItem>
            <Divider />
            <ListItem button>
                <a href="/checkout" className="btn btn-success btn-block btn-lg">
                    Proceed to Checkout
                </a>
            </ListItem>
          </List>
        </div>
    );


    return (
        <>

        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Login</DialogTitle>
            <DialogContent>
            <DialogContentText style={{ color: '#000000' }}>
                Please Logged In to this website, your email and password are end to end encrypted and secure.
            </DialogContentText>
            <TextField style={{ marginTop: '4px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
            <br />
            <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
            
            <Button style={{ marginTop: '14px', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} style={{ backgroundColor: '#000000', color: '#ffffff' }} variant="outlined" fullWidth >Login</Button>
            <center style={{ marginTop: '10px' }}>
                OR
            </center>
            <Button onClick={signIn} style={{ backgroundColor: '#ffffff', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
                Continue with Google
            </Button>
            <Button style={{ backgroundColor: '#ffffff', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
                Continue with Facebook
            </Button>
            
            <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                <a onClick={() => registerOpen('Register')} href="#!">
                Don't have an Account Sign Up
                </a>
            </center>

            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} color="primary">
                Cancel
            </Button>
            </DialogActions>
        </Dialog>

        <Dialog open={openSignUp} onClose={handleCloseSignUp} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
            <DialogContent>
            <DialogContentText style={{ color: '#000000' }}>
                Please Register into this website, your email and password are end to end encrypted and secure.
            </DialogContentText>
            <TextField style={{ marginTop: '4px' }} id="outlined-basic" label="Full Name" type="text" variant="outlined" fullWidth />
            <br />
            <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
            <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
            <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Confirm Password" type="password" variant="outlined" fullWidth />
            
            <Button style={{ marginTop: '14px', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} style={{ backgroundColor: '#000000', color: '#ffffff' }} variant="outlined" fullWidth >Sign Up</Button>
            
            <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                <a onClick={() => registerOpen('Login')} href="#!">
                Already have an account
                </a>
            </center>

            </DialogContent>
            <DialogActions>
            <Button onClick={handleCloseSignUp} color="primary">
                Cancel
            </Button>
            </DialogActions>
        </Dialog>


            <div>
            {['right'].map((anchor) => (
                <React.Fragment key={anchor}>
                    <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                        {list(anchor)}
                    </Drawer>
                </React.Fragment>
            ))}
            </div>
            <header className="header-v2">
                <div className="container-menu-desktop trans-03">
                    <div className="wrap-menu-desktop">
                        <nav className="navbar ">
                            <div className="navbar-brand">
                                <a className="navbar-item" href="/">
                                    <img src="//cdn.shopify.com/s/files/1/2636/1928/files/OCS_EN_LOGO_BLK_SM_nav_600x.png?v=1560138074" alt="Work" />
                                </a>
                                <a className="navbar-item is-hidden-desktop" href="https://github.com/jgthms/bulma" target="_blank">
                                    <span className="icon" style={{ color: '#333' }}>
                                        <i className="fa fa-github"></i>
                                    </span>
                                </a>
                                <a className="navbar-item is-hidden-desktop" href="https://twitter.com/jgthms" target="_blank">
                                    <span className="icon" style={{ color: '#55acee' }}>
                                        <i className="fa fa-twitter"></i>
                                    </span>
                                </a>
                                <div className="navbar-burger burger" data-target="navMenubd-example">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                            <div id="navMenubd-example" className="navbar-menu">
                                <div className="navbar-start">
                                    <div className="navbar-item">
                                        <a className="font-weight-bold" href="/">HOME</a>
                                    </div>
                                    <div className="navbar-item has-dropdown is-hoverable is-mega">
                                        <div className="navbar-link flex font-weight-bold">
                                            FLOWER 
                                        </div>
                                        <div id="blogDropdown" className="navbar-dropdown " data-style={{ width: '132rem;' }}>
                                            <div className="container is-fluid">
                                                <div className="columns">
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Plant Type</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Indica-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=sativa">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Sativa-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Hybrid</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Shop Value Picks</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Price Drops</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Sativa-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Hybrid</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Potency</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`THC > 20%`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Balanced 1:1</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">CBD-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Featured Collections</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`New Flower`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Bestselling</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Shop All Brands</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Potency</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Featured Flower: Tangerine</p>
                                                            </div>
                                                        </a>
                                                        <h1 className="title is-6 is-mega-menu-title">Dream</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`Learn more about this citrusy sativa-dominant strain`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="navbar-divider" />
                                            <div className="navbar-item">
                                                <div className="navbar-content">
                                                    <div className="level is-mobile">
                                                        <div className="level-left">
                                                            <div className="level-item">
                                                                <button className="btn btn-outline-success btn-lg">
                                                                    Shop Now Flower
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="level-right">
                                                            <div className="level-item">
                                                                <a className="button bd-is-rss is-small" href="http://bulma.io/atom.xml">
                                                                    <span className="icon is-small">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="navbar-item has-dropdown is-hoverable is-mega">
                                        <div className="navbar-link flex font-weight-bold">
                                            VAPES 
                                        </div>
                                        <div id="blogDropdown" className="navbar-dropdown " data-style={{ width: '132rem;' }}>
                                            <div className="container is-fluid">
                                                <div className="columns">
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Cartridge Type</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">510 Thread Cartridges</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Closed-Loop Pods</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">1 g Cartridges</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Cartridge Features</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Flavoured Vapes</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Specialty Vapes</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Full Spectrum Vapes</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Potency</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`THC > 20%`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Balanced 1:1</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">CBD-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Featured Collections</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`New Vapes`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Vapes Under $7 / 0.1g</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">True to Flower Vapes</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Vape Price Drops</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">These price drops are in it for the long haul</p>
                                                            </div>
                                                        </a>
                                                        <h1 className="title is-6 is-mega-menu-title"></h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{``}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="navbar-divider" />
                                            <div className="navbar-item">
                                                <div className="navbar-content">
                                                    <div className="level is-mobile">
                                                        <div className="level-left">
                                                            <div className="level-item">
                                                                <button className="btn btn-outline-success btn-lg">
                                                                    Shop Now Vapes
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="level-right">
                                                            <div className="level-item">
                                                                <a className="button bd-is-rss is-small" href="http://bulma.io/atom.xml">
                                                                    <span className="icon is-small">
                                                                        <i className="fa fa-rss"></i>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="navbar-item has-dropdown is-hoverable is-mega">
                                        <div className="navbar-link flex font-weight-bold">
                                            EXTRACTS 
                                        </div>
                                        <div id="blogDropdown" className="navbar-dropdown " data-style={{ width: '132rem;' }}>
                                            <div className="container is-fluid">
                                                <div className="columns">
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Ingestible Extracts</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Bottled Oils</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Oral Sprays</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Capsules</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Concentrates</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Solventless Concentrates</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Concentrate Accessories</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Potency</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`High THC`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Balanced 1:1</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">CBD-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Featured Collections</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`New Extracts`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Sublingual Strips</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Full Spectrum Extracts</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Sublingual Strips</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">For a discreet, easy, <br /> melt-in-your-mouth experience</p>
                                                            </div>
                                                        </a>
                                                        <h1 className="title is-6 is-mega-menu-title"></h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{``}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold"></p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="navbar-divider" />
                                            <div className="navbar-item">
                                                <div className="navbar-content">
                                                    <div className="level is-mobile">
                                                        <div className="level-left">
                                                            <div className="level-item">
                                                                <button className="btn btn-outline-success btn-lg">
                                                                    Shop Now Extracts
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="level-right">
                                                            <div className="level-item">
                                                                <a className="button bd-is-rss is-small" href="http://bulma.io/atom.xml">
                                                                    <span className="icon is-small">
                                                                        <i className="fa fa-rss"></i>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="navbar-item has-dropdown is-hoverable is-mega">
                                        <div className="navbar-link flex font-weight-bold">
                                            EDIBLES 
                                        </div>
                                        <div id="blogDropdown" className="navbar-dropdown " data-style={{ width: '132rem;' }}>
                                            <div className="container is-fluid">
                                                <div className="columns">
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Edible Type</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Soft Chews</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Chocolates</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Baked Goods</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Beverage Type</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Sparkling Water</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Infusible Beverages</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Sodas and Sparkling Beverages</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="column">
                                                        <h1 className="title is-6 is-mega-menu-title">Shop by Potency</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`10 mg THC`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Balanced 1:1</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">CBD-Dominant</p>
                                                            </div>
                                                        </a>
                                                        <hr />
                                                        <h1 className="title is-6 is-mega-menu-title">Featured Collections</h1>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">{`New Edibles and Beverages`}</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Edibles Under $6</p>
                                                            </div>
                                                        </a>
                                                        <a className="navbar-item" href="/products?q=indica">
                                                            <div className="navbar-content">
                                                                <p className="h5 font-weight-bold">Oil and Fat Infusers</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="navbar-divider" />
                                            <div className="navbar-item">
                                                <div className="navbar-content">
                                                    <div className="level is-mobile">
                                                        <div className="level-left">
                                                            <div className="level-item">
                                                                <button className="btn btn-outline-success btn-lg">
                                                                    Shop All Edibles
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="level-right">
                                                            <div className="level-item">
                                                                <a className="button bd-is-rss is-small" href="http://bulma.io/atom.xml">
                                                                    <span className="icon is-small">
                                                                        <i className="fa fa-rss"></i>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="navbar-end">
                                    {
                                        userlogin ? (
                                        <>
                                        <Badge style={{ marginTop: '2vh' }} badgeContent={totalItems} color="secondary">
                                            <IconButton style={{ color: '#000' }} size="large" href="/cart" aria-label="delete">
                                                <ShoppingCartIcon fontSize="large" />
                                            </IconButton>
                                        </Badge>
                                        <Avatar style={{ cursor: 'pointer', marginTop: '3vh', marginLeft: '2vh' }} onClick={() => window.location.href = "/profile"} alt={user.name} src={user.photoURL} />
                                        </>
                                        ) : (
                                        <a className="navbar-item is-hidden-desktop-only" href="#!">
                                            <IconButton onClick={handleClickOpen} aria-label="delete">
                                                <ExitToAppIcon fontSize="large" style={{ color: '#000' }} />
                                            </IconButton>
                                        </a>
                                        )
                                    }
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
        </>
    )
}

export default Navigation
