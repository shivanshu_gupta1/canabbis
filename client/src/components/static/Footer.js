import React from 'react';

const Footer = () => {
    return (
        <>
            <footer class="page-footer text-center text-md-left stylish-color-dark pt-0">
            <div style={{ backgroundColor: 'rgb(47 154 16)' }}>
                <div class="container">
                    <div class="row py-4 d-flex align-items-center">
                        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                            <h6 class="mb-0 white-text font-weight-bold">Get connected with us on social networks!</h6>
                        </div>
                        <div class="col-md-6 col-lg-7 text-center text-md-right">
                            <a class="fb-ic ml-0 px-2"><i class="fab fa-facebook-f white-text"> </i></a>
                            <a class="tw-ic px-2"><i class="fab fa-twitter white-text"> </i></a>
                            <a class="gplus-ic px-2"><i class="fab fa-google-plus-g white-text"> </i></a>
                            <a class="li-ic px-2"><i class="fab fa-linkedin-in white-text"> </i></a>
                            <a class="ins-ic px-2"><i class="fab fa-instagram white-text"> </i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mt-5 mb-4 text-center text-md-left">
                <div class="row mt-3">
                    <div class="col-md-3 col-lg-4 col-xl-3 mb-4">
                        <h6 style={{ color: '#fff' }} class="text-uppercase font-weight-bold text-light"><>Company name</></h6>
                        <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style={{ width: '60px' }} />
                        <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit.</p>
                    </div>
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 style={{ color: '#fff' }} class="text-uppercase font-weight-bold"><>Products</></h6>
                        <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style={{ width: '60px' }} />
                        <p><a href="#!">Product 1</a></p>
                        <p><a href="#!">Product 2</a></p>
                        <p><a href="#!">Product 3</a></p>
                        <p><a href="#!">Product 4</a></p>
                    </div>
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 style={{ color: '#fff' }} class="text-uppercase font-weight-bold"><>Useful links</></h6>
                        <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style={{ width: '60px' }} />
                        <p><a href="#!">Home</a></p>
                        <p><a href="#!">Product</a></p>
                        <p><a href="#!">Shipping Rates</a></p>
                        <p><a href="#!">Help</a></p>
                    </div>
                    <div class="col-md-4 col-lg-3 col-xl-3">
                        <h6 style={{ color: '#fff' }} class="text-uppercase font-weight-bold"><>Contact</></h6>
                        <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style={{ width: '60px' }} />
                        <p><i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
                        <p><i class="fas fa-envelope mr-3"></i> info@example.com</p>
                        <p><i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
                        <p><i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
                    </div>
                </div>
            </div>
            <div class="footer-copyright py-3 text-center">
                <div class="container-fluid">
                    © 2021 Copyright: <a href="#!" target="_blank"> company.com </a>
                </div>
            </div>
            </footer>
        </>
    )
}

export default Footer;
