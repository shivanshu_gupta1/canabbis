import React from 'react';
import Grid from '@material-ui/core/Grid';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
import { indica, sativa } from './items/indica';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    button: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
}));


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ProductList = ({ deleteItem, p, settotalprice, product }) => {
    var sizes = product.product.size[0];
    settotalprice(p);
    return (
        <>
            
        </>
    )
}



function getSteps() {
    return ['Information', 'Payment'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
        return (
            <>
                <div className="row">
                    <div className="col">
                        <h2 className="font-weight-bold">Contact information</h2>
                        <TextField id="outlined-basic" fullWidth label="Email" required variant="outlined" />
                    </div>
                </div>
                <br />
                <h2 className="font-weight-bold">Shipping Address</h2>
                <div className="row">
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="First Name" required variant="outlined" />
                    </div>
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="Last Name" required variant="outlined" />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="Address" required variant="outlined" />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="Apartment, suite, etc. (optional)" variant="outlined" />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="City" required variant="outlined" />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="Province" required variant="outlined" />
                    </div>
                    <div className="col">
                        <TextField id="outlined-basic" fullWidth label="Postal Code" required variant="outlined" />
                    </div>
                </div>
                <br />
                <p>
                Note that products featured on this website can only be delivered to addresses within the Province of Ontario. In order to receive text notifications on the status of your delivery, please enter your mobile number.
                </p>
                <div className="col">
                    <TextField id="outlined-basic" fullWidth label="Phone" required variant="outlined" />
                </div>
                <br />
                <p>
                Please note: Due to COVID-19 precautions, Canada Post has temporarily changed its delivery method. They will not be delivering parcels that require a signature or proof-of-age to the customer’s door.
                </p>
                <br />
                <p>
                OCS.ca will not process transactions to deliver cannabis or related items after receiving and implementing a request from a First Nations band council not to deliver, pursuant to the Ontario Cannabis Retail Corporation Act, 2017, S.O. 2017, c.26, Sched. 2, s.28.1. Personal information will be provided to third parties in order to facilitate payment and delivery. It will not be used by third parties in any other capacity.
                </p>
                <br />
                <p>
                Personal information and transactional data associated with your purchase from OCS.ca is collected under the authority of the Ontario Cannabis Retail Corporation Act, 2017, S.O. 2017, c.26, Sched. 2, s. 4, in order to process and deliver your order, support product quality assurance, provide customer care, complete returns and protect against fraud. Should you have any questions regarding the collection and/or use of this information, please contact the OCS FOI & Privacy Office, 4100 Yonge Street, Toronto, ON, M2P 2B5. Telephone 1-888-910-0627. You may also visit OCS.ca to view the OCS Customer Privacy Policy, and Website Terms and Conditions for further details. Personal information will be provided to third parties in order to facilitate payment and delivery. It will not be used by third parties in any other capacity.
                </p>
            </>
        );
        case 1:
        return (
            <>
                <div className="row">
                    <div className="col">
                        <h2 className="font-weight-bold">Payment Information</h2>
                    </div>
                </div>
                <br />
            </>
        );
        default:
        return 'Unknown step';
    }
}


const Checkout = ({ location }) => {
    const classes = useStyles();
    const [message, setmessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState('');
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const refreshList = () => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }

    const deleteItem = (documentId) => {
        setLoading(true);
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            var items = doc.data().items;
            axios.get(`${API_SERVICE}/api/v1/main/removeitemtocart/${documentId}`)
                .then((response) => {
                    if (response.status === 200) {
                        refreshList();
                        handleClick();
                        setmessage('Item Removed from Cart');
                        items = items - 1;
                        docRef.set({
                            items 
                        }, { merge: true });
                    } 
                }).catch(err => console.log(err));
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const showProductList = () => {
        var p = 0;
        return products.map(product => {
            p = product.product.price + p;
            return <ProductList deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }

    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set());
    const steps = getSteps();

    const isStepOptional = (step) => {
        return step === 1;
    };
    
    const isStepSkipped = (step) => {
        return skipped.has(step);
    };
    
    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
          newSkipped = new Set(newSkipped.values());
          newSkipped.delete(activeStep);
        }
    
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };
    
    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
    
    const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }
    
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
          const newSkipped = new Set(prevSkipped.values());
          newSkipped.add(activeStep);
          return newSkipped;
        });
    };
    
    const handleReset = () => {
        setActiveStep(0);
    };
    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    {message}
                </Alert>
            </Snackbar>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Checkout</h2>
                    <p className="lead font-weight-bold text-success h5">
                        {
                            products && products.length ? (
                            <>    
                                {`Total Price $ ${totalprice} CAD`}
                            </>
                            ) : (
                                `Total Price $ 0 CAD`
                            )
                        }
                    </p>
                </div>
            </div>
            <section className="m-4">
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <Grid container spacing={3}>
                                {showProductList()}

                                <Container>
                                    <Stepper activeStep={activeStep}>
                                        {steps.map((label, index) => {
                                        const stepProps = {};
                                        const labelProps = {};
                                        if (isStepOptional(index)) {
                                            labelProps.optional = <Typography variant="caption"></Typography>;
                                        }
                                        if (isStepSkipped(index)) {
                                            stepProps.completed = false;
                                        }
                                        return (
                                            <Step key={label} {...stepProps}>
                                            <StepLabel {...labelProps}>{label}</StepLabel>
                                            </Step>
                                        );
                                        })}
                                    </Stepper>
                                    <div>
                                        {activeStep === steps.length ? (
                                        <div>
                                            <Typography className={classes.instructions}>
                                            All steps completed - you&apos;re finished
                                            </Typography>
                                            <Button onClick={handleReset} className={classes.button}>
                                            Reset
                                            </Button>
                                        </div>
                                        ) : (
                                        <div>
                                            <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                            <div>
                                            <Button size="large" disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                                                Back
                                            </Button>
                                            <Button
                                                variant="contained"
                                                size="large"
                                                color="primary"
                                                onClick={handleNext}
                                                className={classes.button}
                                            >
                                                {activeStep === steps.length - 1 ? 'Submit' : 'Next'}
                                            </Button>
                                            </div>
                                        </div>
                                        )}
                                    </div>
                                </Container>

                            </Grid>
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        Empty Cart
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
            </section>
            <Footer />
        </>
    )
}

export default Checkout
