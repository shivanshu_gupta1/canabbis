const indica = [
    {
        _id: 1,
        type: 'SIMPLE STASH',
        name: 'Indica Bud',
        category: 'Indica Dominant',
        size: [
            5, 10, 28
        ],
        price: 3.56,
        thc: '12-17',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00688083003783_00_compressed_101563_e021c766-6599-4419-8d4f-85c44db74fd0_480x.jpg'
    },
    {
        _id: 2,
        type: 'Bingo',
        name: 'Ready to Roll Milled Indica',
        category: 'Indica Dominant',
        size: [
            15
        ],
        price: 3.16,
        thc: '14-20',
        cbd: '0-1.99',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00688083003783_00_compressed_101563_e021c766-6599-4419-8d4f-85c44db74fd0_480x.jpg'
    },
    {
        _id: 3,
        type: 'JWC',
        name: 'Select Indica',
        category: 'Indica Dominant',
        size: [
            14,28
        ],
        price: 4.00,
        thc: '15-20',
        cbd: '0-2',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00846067000436_00_compressed_101476_ee19d1f8-b478-4b44-b9d1-20f5cc5a013e_480x.jpg'
    },
    {
        _id: 4,
        type: 'Bingo',
        name: 'Indica!',
        category: 'Indica Dominant',
        size: [
            15,28
        ],
        price: 4.18,
        thc: '13-17',
        cbd: '0-1.99',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00694144009038_00_compressed_101626_480x.jpg'
    },
    {
        _id: 5,
        type: 'Grasslands',
        name: 'Indica!',
        category: 'Indica Dominant',
        size: [
            15,28
        ],
        price: 4.25,
        thc: '14-18',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00827885003472_00_compress_101267_480x.jpg'
    },
    {
        _id: 6,
        type: 'Bingo',
        name: 'Indica!',
        category: 'Indica Dominant',
        size: [
            15
        ],
        price: 4.25,
        thc: '4-10',
        cbd: '5-15',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00694144010034_00_compressed_101782_480x.jpg'
    },
    {
        _id: 7,
        type: 'Orignal Stash',
        name: 'OS.HYBRID',
        category: 'Indica Dominant',
        size: [
            15
        ],
        price: 4.26,
        thc: '14-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00697238113123_00_compress_101791_ca77ec05-75f5-469f-8a71-2133d9a42040_480x.jpg'
    },
    {
        _id: 8,
        type: 'Orignal Stash',
        name: 'OS.HYBRID',
        category: 'Indica Dominant',
        size: [
            15
        ],
        price: 4.29,
        thc: '14-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00697238113123_00_compress_101791_ca77ec05-75f5-469f-8a71-2133d9a42040_480x.jpg'
    },
    {
        _id: 9,
        type: 'Orignal Stash',
        name: 'OS.INDICA',
        category: 'Indica Dominant',
        size: [
            14,28
        ],
        price: 4.29,
        thc: '14-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00697238113185_00_compress_101789_480x.jpg'
    },
    {
        _id: 10,
        type: 'TWD.28',
        name: 'Indica Bud',
        category: 'Indica Dominant',
        size: [
            28
        ],
        price: 4.46,
        thc: '17-23',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00688083003967_00_compressed_101566_0c5bdcfa-b920-4877-bec7-dde9b0b23b3d_480x.jpg'
    },
    {
        _id: 11,
        type: 'Daily Special',
        name: 'Daily Special Indica',
        category: 'Indica Dominant',
        size: [
            3.5,7,15,28
        ],
        price: 4.46,
        thc: '15-22',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00629108141141_00_compress_101306_6be9f883-3f68-4732-97c9-d310ee5227bb_480x.jpg'
    },
    {
        _id: 12,
        type: 'Grassland',
        name: 'Hybrid',
        category: 'Indica Dominant',
        size: [
            7,15
        ],
        price: 4.46,
        thc: '14-19',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00827885003618_00_compress_101269_e67eea82-62ad-40fe-851c-f50185beee0d_480x.jpg'
    }
]



// Sativa

const sativa = [
    {
        _id: 13,
        type: 'SIMPLE STASH',
        name: 'Sativa Bud',
        category: 'Sativa Dominant',
        size: [
            5, 10, 28
        ],
        price: 3.57,
        thc: '12-17',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00688083003790_00_compressed_101564_ba1a55f3-9476-46ac-aae0-3e4545ede5fb_480x.jpg'
    },
    {
        _id: 14,
        type: 'Bingo',
        name: 'Ready to Roll Milled Sativa',
        category: 'Sativa Dominant',
        size: [
            15
        ],
        price: 3.57,
        thc: '14-20',
        cbd: '0-1.99',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00694144008994_00_compress_101704_82b18f51-f6dd-4cff-8926-5d80b4887f66_480x.jpg'
    },
    {
        _id: 15,
        type: 'Bingo',
        name: 'Sativa',
        category: 'Sativa Dominant',
        size: [
            15,28
        ],
        price: 4.18,
        thc: '13-17',
        cbd: '0-1.99',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00694144008956_00_compressed_101627_480x.jpg'
    },
    {
        _id: 16,
        type: 'Pure Sunfarms',
        name: 'Sativa',
        category: 'Sativa Dominant',
        size: [
            28
        ],
        price: 4.20,
        thc: '14-19',
        cbd: '0-1.99',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00628188000652_00_compressed_101361_94b56590-c320-4cc1-9b2c-7fed9a17d8c8_480x.jpg'
    },
    {
        _id: 17,
        type: 'Grasslands',
        name: 'Sativa',
        category: 'Sativa Dominant',
        size: [
            7,15,28
        ],
        price: 4.25,
        thc: '14-19.5',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00827885003540_00_compress_101268_81aeb87d-7947-4b37-8146-7ded5b1114bd_480x.jpg'
    },
    {
        _id: 18,
        type: 'Grasslands',
        name: 'OS.SATIVA',
        category: 'Sativa Dominant',
        size: [
            3.5,14,28
        ],
        price: 4.25,
        thc: '14-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00827885003540_00_compress_101268_81aeb87d-7947-4b37-8146-7ded5b1114bd_480x.jpg'
    },
    {
        _id: 19,
        type: 'TWD.28',
        name: 'Sativa Bud',
        category: 'Sativa Dominant',
        size: [
            28
        ],
        price: 4.46,
        thc: '14-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00688083003950_00_compressed_101565_480x.jpg'
    },
    {
        _id: 20,
        type: 'Hiway',
        name: 'Sativa Pre Roll',
        category: 'Sativa Dominant',
        size: [
            '2 x 1'
        ],
        price: 4.97,
        thc: '12-18',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00628242510660_a1c1_compress_101637_c28cc8db-d7a6-4a0f-b1f1-e5dd647a5bb3_480x.jpg'
    },
    {
        _id: 21,
        type: 'Trailblazer',
        name: 'Spark Buds',
        category: 'Sativa Dominant',
        size: [
            3.5,7,15
        ],
        price: 5.00,
        thc: '15-19',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00671148401273_00_compressed_101399_9f10ef7e-210d-4cc9-92f2-923f08241f00_480x.jpg'
    },
    {
        _id: 22,
        type: 'Buds',
        name: 'BLUBERRY x HAZE',
        category: 'Sativa Dominant',
        size: [
            28
        ],
        price: 5.14,
        thc: '15-20',
        cbd: '0-1',
        photo: '//cdn.shopify.com/s/files/1/2636/1928/products/00671148601024_00_compressed_101402_8273fe3b-1fca-477b-98ab-b671634eb5f7_480x.jpg'
    }
]

export {
    indica,
    sativa
}
