import React from 'react'
import Navigation from './static/Navigation';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { auth, storage } from '../Firebase/index';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import { v4 as uuid4 } from 'uuid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
}));


const Profile = () => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [message, setmessage] = React.useState(false);
    const [user, setUser] = React.useState({});
    const [fullName, setFullName] = React.useState('');
    const [email, setEmail] = React.useState('');

    const [phoneno, setphoneno] = React.useState('');
    const [address, setaddress] = React.useState('');
    const [zipcode, setzipcode] = React.useState('');


    const [verifyEmail, setVerifyEmail] = React.useState(false);
    const [file, setFile] = React.useState([]);
    const [btnMessage, setBtnMessage] = React.useState('Change Profile Picture');
    const [loading, setLoading] = React.useState(true);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
 
    

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
            if (user) {
                setUser(user);
                setFullName(user.displayName);
                setEmail(user.email);
                if (!user.emailVerified) {
                    setVerifyEmail(true);
                }
                setLoading(false);
                axios.get(`${API_SERVICE}/api/v1/main/getuserdataaddress/${user.email}`)
                    .then((response) => {
                        if (response.status === 200) {
                            setphoneno(response.data[0].phoneno);
                            setzipcode(response.data[0].zipcode);
                            setaddress(response.data[0].address);
                            setFullName(response.data[0].fullName);
                        } else {
                            setphoneno('');
                            setzipcode('');
                            setaddress('');
                        }
                    }).catch(err => console.log(err));
            } else {
                console.log("No");
            }
        });
    }, []);

    const refreshUsers = () => {
        axios.get(`${API_SERVICE}/api/v1/main/getuserdataaddress/${user.email}`)
            .then((response) => {
                if (response.status === 200) {
                    setphoneno(response.data[0].phoneno);
                    setzipcode(response.data[0].zipcode);
                    setaddress(response.data[0].address);
                    setFullName(response.data[0].fullName);
                } else {
                    setphoneno('');
                    setzipcode('');
                    setaddress('');
                }
            }).catch(err => console.log(err));
    }

    React.useEffect(() => {
        if (file.length > 0) {
            onSubmit();
        } else {
            console.log("N");
        }
    }, [file]);

    const onSubmit = () => {
        if (file.length > 0) {
            file.forEach(file => {
                var uniquetwoKey = uuid4() + Date.now();
                const uploadTask = storage.ref(`profilepic/${uniquetwoKey}/${file.name}`).put(file);
                uploadTask.on('state_changed', (snapshot) => {
                    const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    setBtnMessage(`Uploading ${progress} %`);
                    
                },
                (error) => {
                    console.log(error);
                },
                async () => {
                    // When the Storage gets Completed
                    const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                    setBtnMessage('Upload Photo');
                    setTimeout(() => setFile([]), 1000);
                    auth.onAuthStateChanged(function(user) {
                        if (user) {
                            user.updateProfile({
                                photoURL: filePath
                            }).then(function() {
                                // Update successful.
                            }).catch(function(error) {
                                // An error happened.
                            });
                        } else {
                            console.log("No");
                        }
                    });
                });
            })
        }
    }

    const logOut = () => {
        auth.signOut().then(function() {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            window.location.href = "/";
        }).catch(function(error) {
            console.log(error);
        });
    }

    // Style Avatar
    const avatar = {
        verticalAlign: 'middle',
        width: '150px',
        height: '150px',
        borderRadius: '50%',
        marginLeft: '8%',
        marginBottom: '4px'
    }

    const handleDrop = async (acceptedFiles) => {
        setFile(acceptedFiles.map(file => file));
    }

    const updateUserProfile = () => {
        var email = user.email;
        var uploadData = {
            fullName,
            phoneno,
            address,
            zipcode,
            email
        }
        axios.post(`${API_SERVICE}/api/v1/main/updateprofile`, uploadData)
            .then((response) => {
                if (response.status === 200) {
                    handleClose();
                    refreshUsers();
                } 
            }).catch(err => console.log(err));
    }

    return (
        <>
        
        <Dialog
            open={open}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">
                Please provide your correct address this will be used for shipping and contacts.
            </DialogTitle>
            <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
                <TextField
                    id="outlined-helperText"
                    label="Receivers Name"
                    style={{ marginTop: '10px' }}
                    variant="outlined"
                    fullWidth
                    value={fullName}
                />

                <TextField
                    id="outlined-helperText"
                    label="Phone No."
                    style={{ marginTop: '10px' }}
                    variant="outlined"
                    fullWidth
                    value={phoneno}
                    onChange={(event) => setphoneno(event.target.value)}
                />
                
                <TextField
                    id="outlined-helperText"
                    label="Address"
                    style={{ marginTop: '10px' }}
                    variant="outlined"
                    fullWidth
                    value={address}
                    onChange={(event) => setaddress(event.target.value)}
                />

                <TextField
                    id="outlined-helperText"
                    label="ZIP Code"
                    style={{ marginTop: '10px' }}
                    variant="outlined"
                    fullWidth
                    value={zipcode}
                    onChange={(event) => setzipcode(event.target.value)}
                />
            </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} color="primary">
                Close
            </Button>
            <Button onClick={updateUserProfile} color="primary">
                Submit
            </Button>
            </DialogActions>
        </Dialog>

        <CssBaseline />
            <Navigation />
            <Container style={{ marginTop: '2%' }}>
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        <div className={classes.root}>
                            <Grid container spacing={3}>
                                <Grid item xs>
                                    <center>
                                        <img src={user.photoURL} alt="File Adventure Avatar" style={avatar} />
                                            {
                                                <Dropzone onDrop={handleDrop}>
                                                    {({ getRootProps, getInputProps }) => (
                                                        <div {...getRootProps({ className: "dropzone" })}>
                                                        <input {...getInputProps()} />
                                                            <Button style={{ marginLeft: '8%' }}  color="primary">
                                                            {
                                                                file.length > 0 ? (
                                                                    <>
                                                                        {btnMessage}
                                                                    </>
                                                                ) : (
                                                                    <>
                                                                        {btnMessage}
                                                                    </>
                                                                )
                                                            }
                                                            </Button>
                                                        </div>
                                                    )}
                                                </Dropzone>
                                            }
                                    </center>
                                </Grid>
                                <Grid item xs>
                                    <h1 style={{ fontWeight: 'bold' }}>
                                        Welcome {fullName}
                                    </h1>
                                    <br />
                                    <TextField
                                        id="outlined-number"
                                        label="Display Name"
                                        type="text"
                                        variant="outlined"
                                        fullWidth
                                        value={fullName}
                                        onChange={(event) => setFullName(event.target.value)}
                                        style={{ marginBotom: '10px' }}
                                    />
                                    <TextField
                                        id="outlined-number"
                                        label="Email"
                                        type="email"
                                        variant="outlined"
                                        fullWidth
                                        value={email}
                                        style={{ marginTop: '10px' }}
                                        disabled
                                    />
                                    <Button size="large" style={{ backgroundColor: '#000000', color: '#ffffff', marginTop: '10px' }} variant="outlined" fullWidth >Update Profile</Button>
                                    <Button onClick={logOut} size="large" style={{ marginTop: '10px' }} color="secondary" variant="outlined" fullWidth >Logout</Button>
                                </Grid>
                            </Grid>
                        </div>
                    )
                }

                <h2 className="font-weight-bold mt-2 mb-2">Orders</h2>
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action">
                    <i class="fas h4 text-success fa-shipping-fast mr-2"></i>
                    Your Orders
                    </a>
                    <a href="#" onClick={handleClickOpen} class="list-group-item list-group-item-action">
                    <i class="fas h4 text-primary fa-house-user mr-2"></i>
                    Your Address
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                    <i class="fas h4 text-danger fa-heart mr-2"></i>
                    Wishlist
                    </a>
                </div>
                <br />
            </Container>
        </>
    )
}

export default Profile
