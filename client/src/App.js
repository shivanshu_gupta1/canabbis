import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Components
import Home from './components/Home';
import Products from './components/Products';
import Checkout from './components/Checkout';
import Cart from './components/Cart';
import Admin from './components/admin/SignIn';
import Profile from './components/Profile';
import AdminDashboard from './components/admin/Orders';
import OrderDetails from './components/admin/OrderInfo';


function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/products" exact component={Products} />
      <Route path="/checkout" exact component={Checkout} />
      <Route path="/cart" exact component={Cart} />
      <Route path="/admin" exact component={Admin} />
      <Route path="/profile" exact component={Profile} />
      <Route path="/admin/orders" exact component={AdminDashboard} />
      <Route path="/admin/orderinfo" exact component={OrderDetails} />
    </Router>
  );
}

export default App;
